class House{
    var house_id:Int = 1
    var housing_id:Int = 123
    var name:String=""
    var address:String=""
    var latitude:Double=0.1
    var longtude:Double=0.0

}
fun main(args: Array<String>){
    val h = House()
    h.house_id = 1
    h.housing_id = 123
    h.name = "Pesona Indah"
    h.address = "Jl.Raya Laswi"
    h.latitude = -7.66519
    h.longtude = 111.31629

    println ("Data com.example.kotlinproject.Mahasiswa : ${h.house_id},${h.housing_id},${h.name},${h.address},${h.latitude}${h.longtude}")
}
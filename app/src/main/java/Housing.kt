class Housing{
    var housing_id:Int = 123
    var developer_id:Int = 1
    var name:String=""
    var address:String=""
    var latitude:Double=0.1
    var longtude:Double=0.0

}
fun main(args: Array<String>){
    val h = Housing()
    h.housing_id = 123
    h.developer_id = 1
    h.name = "Pesona Indah"
    h.address = "Jl.Raya Laswi"
    h.latitude = -7.66519
    h.longtude = 111.31629

    println ("Data com.example.kotlinproject.Mahasiswa : ${h.housing_id},${h.developer_id},${h.name},${h.address},${h.latitude}${h.longtude}")
}
package com.example.kotlinproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_main.*

class Home : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)


        btn_start.setOnClickListener() {
            val firstteam = txtFirstTeamName.text
            val secondteam = txtSecondTeam.text

            Log.d("Intention","First")


            //validasi
            if (firstteam.length == 0 || secondteam.length == 0) {
                Toast.makeText(applicationContext, "Please input Field First", Toast.LENGTH_LONG).show()
            } else {
                val pindah: Intent = Intent(applicationContext, MainActivity::class.java)
                //data
                startActivity(pindah)
            }
        }
    }
}




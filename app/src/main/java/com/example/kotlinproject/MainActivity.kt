package com.example.kotlinproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {

    var scoreFirstTeam : Int = 0
    var scoreSecondTeam : Int = 0

    fun addPoint(team : Int,point : Int){
        if(team == 1) {
            scoreFirstTeam = scoreFirstTeam + point
        }else{

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d("", "oncreate called!")

    }

    override fun onStart() {
        super.onStart()
        Log.d("", "onstart called!")

    }

    override fun onResume() {
        super.onResume()
        Log.d("","onresume called!")
    }

    override fun onPause() {
        super.onPause()
        Log.d("","onpause called!")
    }


    override fun onStop() {
        super.onStop()
        Log.d("","onstop called!")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("", "ondestroy called!")
    }
}
